% GPSmonthavg is intended to create monthly least squares averages of the daily 
% gps data, after it has been processes through addoffsets2res.m
%
% First edited 7/28/20 - LAK

clear all;

% Get site list
fid = fopen('siteidlist.txt');
s = textscan(fid, '%s');
fclose(fid);
sites = string(s{1});
Nsites = length(sites);

for i = 1:Nsites
    sid = sites(i);
    fprintf('Adding offsets to station %s time series\n',sid);
    
    % Load GPS observations
    fname = sprintf('off_files/%s_OFF.dat3',sid);
    dat = load(fname);

    % Get data from file
    x_obs = dat(:,2);
    s_obs = dat(:,3);

    % Get time vector
    t = (round(dat(:,1),4));
    tnum = decyear2matlabdate(t);
    tvec = datevec(tnum);
    tvec(:,4:6) = 0;

    % Initialize starting month, day, year
    oyr = tvec(:,1);
    om = tvec(:,2);
    od = tvec(:,3);

    %Initialze all other variables
    d = 0;
    s = 0;
    mm_old = om(1);
    yy_old = oyr(1);
    YA = [];
    MA = [];
    obs = [];
    sig = [];
    XA = [];
    SA = [];
    id = 0;
    
    % Loop through days 
    fprintf('Calculating weighted montly averages for %s\n',sid)
    for im = 1:length(om)
        % While in the same month, add the observations from that month to an array
        if om(im) == mm_old 
            id = id + 1;
            s = s_obs(im);
            sig = [sig; s];
            d = x_obs(im);
            obs = [obs; d];
        % Once the date has moved to the next month, find the average for that month
        else
            Y = yy_old;
            YA = [YA; Y];
            M = mm_old;
            MA = [MA; M];

            % We're doing Least Squares averaging
            var = sig.^2;
            W = inv(diag(var));
            A  = (ones(length(W),1));
            C = inv(A' * W * A);
            save C.mat 'C'
            X = C * A' * W * obs;
            res = obs - A * X;
            frac = (1/(id - 1));
            nrms = sqrt(frac * sum((res.^2)./var));
            
            % We also scale the computed uncertainty by the normalized root mean square error
            S = nrms*sqrt(C);

            XA = [XA; X];
            SA = [SA; S];
            
            % Clear values
            sig = [];
            obs = [];
            d = 0;
            s = 0;
            id = 0;
            R = 0;
            
            % Initialize variables for the new month
            id = id + 1;
            mm_old = om(im);
            yy_old = oyr(im);
            s = s_obs(im);
            sig = [sig; s];
            d = x_obs(im);
            obs = [obs; d];
	end
    end
    
    % Do averaging on the last month in the time series
    Y = yy_old;
    YA = [YA; Y];
    M = mm_old;
    MA = [MA; M];
    var = sig.^2;
    W = inv(diag(var));
    A  = (ones(length(W),1));
    C = inv(A' * W * A);
    X = C * A' * W * obs;
    res = obs - A * X;

    frac = (1/(id - 1));
    nrms = sqrt(frac * sum((res.^2)./var));

    S = nrms*sqrt(C);
            
    XA = [XA; X];
    SA = [SA; S];
    
    % Create the date vector for the monthly data. We set the day of each month to the 15th
    d = ones(length(YA),1)*15;
    h = zeros(length(YA),1);
    m = zeros(length(YA),1);
    s = zeros(length(YA),1);
    avgdate = decyear(YA,MA,d,h,m,s);
    
    % Get rid of any data points that have NaN as the uncertainty      
    nanum = find(isnan(SA));   
    XA(nanum) = [];
    avgdate(nanum) = [];
    SA(nanum) = [];
    
    % Use matlab's built-in "isoutlier" function to remove any obvious outliers    
    out = find(isoutlier(SA,'mean'));
    XA(out) = [];
    avgdate(out) = [];
    SA(out) = [];
   
    % Save monthly averages file
    filename = sprintf('avg_files/%s_AVG.dat3',sid);
    fid = fopen(filename, 'w');
    fprintf(fid,'%4.4f %6.8f %6.8f\n',[avgdate,XA,SA]');
    fclose(fid); 
end

    
