% A script to go to UNR and download the .tenv files for specified stations
% in the IGS14 reference frame

% Created 1/15/19 LAK

clear all

% Get up-to-date step list
url = 'http://geodesy.unr.edu/NGLStationPages/steps.txt';
fname = 'unrsteps.txt';
websteps = webread(url);
mastersteps = splitlines(convertCharsToStrings(websteps));

% Get site list
fid = fopen('siteidlist.txt');
s = textscan(fid, '%s');
fclose(fid);
sites = string(s{1});
Nsites = length(sites);

for i = 1:Nsites
    sid = sites(i);
    equip_step = [];
    earthq_step = [];
    vecdates = [];
    
    k = contains(mastersteps, sid);
    rownum = find(k == 1);
    for iR = 1:length(rownum)
        siteoff = split(mastersteps(rownum(iR)));
        stepdates = mjuliandate(siteoff(2), 'yymmmdd');
        siteoff(2) = stepdates;
        if length(siteoff) > 4
            earthq_step = [earthq_step; strjoin(siteoff')];
        else
            siteoff = [siteoff; 0; 0; 0];
            equip_step = [equip_step; strjoin(siteoff')];
        end
    end
    sitestep = [equip_step; earthq_step];
    
    txtstepsf = sprintf('site_steps/%s_step_info.txt', sid);
    fid = fopen(txtstepsf,'w');
    fprintf(fid,'%s\n', sitestep);
    fclose(fid);
end
