% env2dat takes .tenv files from UNR and converts them into .dat and .mom
% files. Dat files are typically used in any matlab analysis. Mom files are
% intended to be used with Hector software.

% First edited 1/15/19 - LAK

clear all

% Get site list
fid = fopen('siteidlist.txt');
s = textscan(fid, '%s');
fclose(fid);
sites = string(s{1});
Nsites = length(sites);

% Loop through .tenv3 files
for i = 1:Nsites
    sid = sites(i);
    fprintf('Creating residual "dat" and "mom" files for station %s\n', sid)
    envfname = sprintf('env_files/%s.tenv3',sid);
    dat = importdata(envfname);
    env = dat.data;
    
    stepfname = sprintf('site_steps/%s_step_info.txt', sid);
    stepfid = fopen(stepfname);
    stepdat = textscan(stepfid, '%s %s %s %s %s %s %s\n');
    fclose(stepfid);
    steps = string(stepdat{1,2})';
    
    if ~isempty(steps)
        offepoch = unique(str2double(steps));
    end
   
    % Dates are in modified julian date
    mjdate = mjuliandate(datevec(decyear2matlabdate(env(:,1))'));
    
    % Set dates to be hour 0 minute 0 of the day's observations
    dt = datetime(datevec(decyear2matlabdate(env(:,1))'));
    
    for iT = 1:length(dt)
        if dt(iT).Hour == 23
            dt(iT) = dateshift(dt(iT),'start','day','nearest');
        end
    end
    dt = dateshift(dt,'start','day','current');
    date = round(decyear(dt),4);
    
    % Get north, east, and vertical residuals (mean removed)
    n_mean = round(mean(env(:,9)),6);
    n = (env(:,9) - n_mean)*1000; n = round(n,3);
    e_mean = round(mean(env(:,7)),6);
    e = (env(:,7) - e_mean)*1000; e = round(e,3);
    v_mean = round(mean(env(:,11)),6);
    v = (env(:,11) - v_mean)*1000; v = round(v,3);
    
    % Get north, east, and vertical uncertainties
    nsig = env(:,14)*1000;
    esig = env(:,13)*1000;
    vsig = env(:,15)*1000;
    
    % Residual north
    fname = sprintf('res_files/dat/%s_RES.dat1', sid);
    fid = fopen(fname,'w');
    fprintf(fid,'%4.4f  %6.3f  %6.3f\n', [date, n, nsig]');
    fclose(fid);
    
    fname2 = sprintf('res_files/mom/%s_RES_N.mom',sites(i,:));
    fid2 = fopen(fname2, 'w');
    fprintf(fid2,'# sampling period 1.0\n');
    if ~isempty(steps)
        fprintf(fid2,'# offset %5.1f\n', offepoch);
    end
    fprintf(fid2,'%5.4f %6.3f\n',[mjdate,n]');
    fclose(fid2);
   
    % Residual east
    fname = sprintf('res_files/dat/%s_RES.dat2', sid);
    fid = fopen(fname,'w');
    fprintf(fid,'%4.4f  %6.3f  %6.3f\n', [date, e, esig]');
    fclose(fid);
    
    fname2 = sprintf('res_files/mom/%s_RES_E.mom',sites(i,:));
    fid2 = fopen(fname2, 'w');
    fprintf(fid2,'# sampling period 1.0\n');
    if ~isempty(steps)
        fprintf(fid2,'# offset %5.1f\n', offepoch);
    end
    fprintf(fid2,'%5.4f %6.3f\n',[mjdate,e]');
    fclose(fid2);
    
    % Residual vertical (also sometimes called "up")
    fname = sprintf('res_files/dat/%s_RES.dat3', sid);
    fid = fopen(fname,'w');
    fprintf(fid,'%4.4f  %6.3f  %6.3f\n', [date, v, vsig]');
    fclose(fid);
    
    fname2 = sprintf('res_files/mom/%s_RES_V.mom',sites(i,:));
    fid2 = fopen(fname2, 'w');
    fprintf(fid2,'# sampling period 1.0\n');
    if ~isempty(steps)
        fprintf(fid2,'# offset %5.1f\n', offepoch);
    end
    fprintf(fid2,'%5.4f %6.3f\n',[mjdate,v]');
    fclose(fid2);
end
