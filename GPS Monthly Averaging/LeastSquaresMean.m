function [LS_mean, LS_sig, nrms] = LeastSquaresMean(obs_in, sig_in)

N = length(obs_in);
var = sig_in.^2;
obs = obs_in./var;
wgt = 1.0./var;
m = sum(obs)/sum(wgt);
s = sqrt(1.0/sum(wgt));

res = obs_in - m;
rw2 = res.^2 ./ var;
nrms = sqrt( 1.0/(N-1.0)*sum(rw2) );
scaled_s = s*nrms;

LS_mean = m;
LS_sig = scaled_s;
end