function num = decyear2matlabdate(y)
% Converts serial (or decimal) year to a standard Matlab date
% Retrieved from Matlab MathWorks forum post to Matlab Answers. User
% "Geoff" provided a version of this function in response to a question.
% https://www.mathworks.com/matlabcentral/answers/31284-converting-decimal-year-into-julian-day-or-a-y-m-d-vector
  Ny = length(y);
  for i= 1:Ny
      format long g;
      year = floor(y(i));
      partialYear = mod(y(i),1);
      date0 = datenum(num2str(year),'yyyy');
      date1 = datenum(num2str(year+1),'yyyy');
      daysInYear = date1 - date0;
      num(i) = date0 + partialYear * daysInYear;
  end
end