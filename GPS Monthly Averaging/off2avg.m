% Read site list (assumed to be within this folder). Generate 
% vector "sites" with dat filenames. Create a variable for the # of sites

% First edited 1/15/19 - LAK
clear all

% Get site list
fid = fopen('siteidlist.txt');
sitefile = textscan(fid, '%s');
fclose(fid);
sites = string(sitefile{1});
Nsites = length(sites);

for i = 1:Nsites
    sid = sites(i);
    fprintf('Creating monthly averaged time series for station %s\n',sid);
    
    % Load GPS observations
    offfname = sprintf('off_files/%s_OFF.dat3',sid);
    offdat = load(offfname);

    %Get obs and sigmas
    obs = offdat(:,2);
    sig = offdat(:,3);
    
    % Adjust dates to be hour 0, minute 0, second 0 of day of observation
    dt = datetime(datevec(decyear2matlabdate(offdat(:,1))'));
    
    for iT = 1:length(dt)
        if dt(iT).Hour == 23
            dt(iT) = dateshift(dt(iT),'start','day','nearest');
        end
    end
    dt = dateshift(dt,'start','day','current');
    tvec = datevec(dt);
    
    yr = tvec(:,1);
    mon = tvec(:,2);
    day = tvec(:,3);
    
    %Initialze first month and year values in time series
    current_month = mon(1);
    current_year = yr(1);
    
    % Initialize arrays to hold important values
    Msig = []; Mobs = [];
    Y = []; M = []; O = []; S = [];
    
    % Initialize days-in-month counter
    dim = 0;
    
    for iM = 1:length(mon)
        % Start with first month of the entire time series, loop until
        % reaching the next month
        if mon(iM) == current_month 
            dim = dim + 1; % Days in month
            
            % Variable Msig will fill up with the current month's sigma
            % values
            day_sig = sig(iM); 
            Msig = [Msig; day_sig];
            
            % Variable Mobs will fill up with the current month's
            % observations
            day_obs = obs(iM);
            Mobs = [Mobs; day_obs];
        else % Once the first of the next month has been reached, perform the following
            % Only proceed if there is more than one observation in the
            % month
            num_obs = length(Mobs);
            if num_obs > 1
                % Set year and month to the current month being assesed
                Y = [Y; current_year];
                M = [M; current_month];
            
                % Use function LeastSquaresMean to obtain monthly average and
                % scale the sigma values
                [avg_obs, avg_sig] = LeastSquaresMean(Mobs, Msig);
            
                O = [O; avg_obs];
                S = [S; avg_sig];
            end
            
            % Reset values
            Msig = []; Mobs = []; dim = 1; 
            
            current_month = mon(iM);
            current_year = yr(iM);
           
            % Start assessing next month, now current month
            day_sig = sig(iM);
            Msig = [Msig; day_sig];
            day_obs = obs(iM);
            Mobs = [Mobs; day_obs];
        end
    end
    
    % Add last month in time series to relevant arrays
    Y = [Y; current_year];
    M = [M; current_month];
    
    [avg_obs, avg_sig] = LeastSquaresMean(Mobs, Msig);        
    O = [O; avg_obs];
    S = [S; avg_sig];
    
    % Get the middle of the month for each month in time series
    mon_end = eomday(Y,M);
    mid_month = mon_end./2;
    
    D = []; H = [];
    for iD = 1:length(mid_month)
        if mod(mid_month,1) ~= 0 % Check if there is a 0.5 after the day
            D = [D; floor(mid_month(iD))];
            H = [H; 12];    
        else
            D = [D; mid_month(iD)];
            H = [H; 0];  
        end
    end
    
    MINUTE = zeros(length(Y),1);
    SECOND = zeros(length(Y),1);
    
    avg_t = decyear(Y,M,D,H,MINUTE,SECOND);
    avg_t = round(avg_t,4);

    out = find(isoutlier(S,'mean'));
    O(out) = [];
    avg_t(out) = [];
    S(out) = [];
    
    %Save averages file
    fname = sprintf('avg_files/%s_AVG.dat3', sid);
    fid = fopen(fname,'w');
    fprintf(fid,'%4.4f  %6.8f  %6.8f\n', [avg_t, O, S]');
    fclose(fid);
end

    
