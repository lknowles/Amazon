% A script to go to UNR and download the .tenv files for specified stations
% in the IGS14 reference frame

% Created 1/15/19 LAK

clear all

% Get site list
fid = fopen('siteidlist.txt');
s = textscan(fid, '%s');
fclose(fid);
sites = string(s{1});
Nsites = length(sites);

for i = 1:Nsites
    sid = sites(i);
    fprintf('Downloading file %s.tenv3 from UNR\n', sid);
    url = sprintf('http://geodesy.unr.edu/gps_timeseries/tenv3/IGS14/%s.tenv3', sid);
    fname = sprintf('env_files/%s.tenv3', sid);
    websave(fname, url);
end
