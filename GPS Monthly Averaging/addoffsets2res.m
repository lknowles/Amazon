% addoffsets2res is intended to add offsets calculated by hector to the
% time series of gps observations
%
% First edited 1/16/19 - LAK

clear all

% Get site list
fid = fopen('siteidlist.txt');
s = textscan(fid, '%s');
fclose(fid);
sites = string(s{1});
Nsites = length(sites);

for i = 1:Nsites
    sid = sites(i);
    fprintf('Adding offsets to station %s time series\n',sid);
    
    % Load GPS observations
    resfname = sprintf('res_files/dat/%s_RES.dat3',sid);
    resdat = load(resfname);
    
    % Load GPS offset estimates
    off_fname = sprintf('offsets/%s_RES.offsets3',sid);
    offdat = load(off_fname);
    
    % Set variables
    t = resdat(:,1);
    d = resdat(:,2);
    sigma = resdat(:,3);
    
    Nd = length(d);
    
    % Put offsets in meters
    if ~isempty(offdat)
        % off_t = decyear(datetime(offdat(:,1),'ConvertFrom','modifiedjuliandate'));
        off_t = offdat(:,1);
        off_d = offdat(:,2);
        off_sigma = offdat(:,3);
        Noff = length(off_d);
        for ioff = 1:Noff
            fprintf('Adding offset number %d\n', ioff);
            for id = 1:Nd
                if (t(id) >= off_t(ioff))
                    d(id) = d(id) - off_d(ioff);
                    %sigma(id) = sqrt((sigma(id)^2) + (off_s(ioff)^2));
                end
            end
        end
    end

    TF = isoutlier(d);
    k = find(TF);
    d(k) = []; t(k) = []; sigma(k) = [];
           
    filename = sprintf('off_files/%s_OFF.dat3',sid);
    fid = fopen(filename, 'w');
    fprintf(fid,'%4.4f %6.8f %6.8f\n',[t,d,sigma]');
    fclose(fid); 
end
